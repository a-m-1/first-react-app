import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Counter.css';

class Counter extends Component {

    constructor(props){
        super(props);
    //     this.state={
    //         score:this.props.initialScore
    //     }
    // }
    // incrementScore=()=>{
    //     this.setState({
    //         score:(this.state.score+1)
    //     });
    // }
    // decrementScore=()=>{
    //     this.setState({
    //         score:(this.state.score-1)
    //     });
    }
    render() {
        return (
            <div className="counter">
                <button className="counter-action decrement" onClick={()=>{this.props.onChange(-1)}}>-</button>
                <div className="counter-score">{this.props.score}</div>
                <button className="counter-action increment" onClick={()=>{this.props.onChange(1)}}>+</button>
            </div>   
        );
    }
}
Counter.propTypes = {
    score: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};
export default Counter;
